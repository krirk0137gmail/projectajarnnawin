package com.akko.poiprojectjobnavigator;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Process;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;
import android.widget.TextView;

//import java.security.Permissions;
import com.akko.poiprojectjobnavigator.Wifilibrary.WifiActivity;

import java.util.Locale;

// This class is first menu
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "NavCamDisActivity";
    private Button setting, nav, rec, del, share;
    private boolean mShowPermissionSlide;
    //  static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1000;

    private boolean isDialogDisabled = true;

    //new talk
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 001;
    private boolean isExploreByTouchEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nav = (Button) findViewById(R.id.navBtn);
        rec = (Button) findViewById(R.id.recBtn);
        setting = (Button) findViewById(R.id.settingฺBtn);
        del = (Button) findViewById(R.id.delBtn);
        share = (Button) findViewById(R.id.shareBtn);
        checkTalkbackEnable();
        //new talk
       AccessibilityManager am = (AccessibilityManager) getSystemService(ACCESSIBILITY_SERVICE);
       isExploreByTouchEnabled = am.isTouchExplorationEnabled(); //Talkback เปิดหรือไม่


      //  Bundle bundle = getIntent().getExtras();
//ทำหน้าที่เหมือน printf ออกมา แล้วดูผ่าน log.d Deb
            //Log.d(TAG, "onCreate: " + bundle.getString("des"));





        if (!validLocationPermission()) requestLocationPermission();
//      Old permission
//      mShowPermissionSlide = !Permissions.haveStoragePermission(this);
//      if (mShowPermissionSlide) {
//          Permissions.requestStoragePermission(this);
//      }
//      Locale.getDefault().getDisplayLanguage();

//      SharedPreferencesUtil.addValue(MainActivity.this, "LANG", Locale.getDefault().getDisplayLanguage());
//      if(SharedPreferencesUtil.getValue(MainActivity.this,"LANG").equals("ENGLISH")){

//
//        if (Locale.getDefault().getDisplayLanguage().equals("ไทย")) {
//            rec.setText("บันทึกเส้นทาง");
//        } else { rec.setText("RECORD PATH"); }


//      Auto detect language
        if (Locale.getDefault().getDisplayLanguage().equals("ไทย")) {
//          share.setText("SHARE PATH");
//          share.setText(Locale.getDefault().getDisplayLanguage());
            share.setText("แชร์เส้นทาง");
            setting.setText("ตั้งค่า");
            del.setText("ลบเส้นทาง");
            nav.setText("เริ่มนำทาง");
            rec.setText("บันทึกเส้นทาง");

        } else {
//          share.setText("แชร์เส้นทาง");
//          share.setText(Locale.getDefault().getDisplayLanguage());
            share.setText("SHARE PATH");
            setting.setText("SETTING");
            del.setText("DELETE PATH");
            nav.setText("NAVIGATION");
            rec.setText("RECORD PATH");
        }


//      Set button menu
        nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SelectDesActivity.class);
                startActivity(intent);
                finish();
            }
        });

        rec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RecordActivity.class);
                startActivity(intent);
                finish();
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);
                finish();
            }
        });

        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DeleteActivity.class);
                startActivity(intent);
                finish();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//              ทำการข้ามส่วนหน้าทดสอบไปหน้าเลือกจับคู่อุปกรณ์เลย
              Intent intent = new Intent(MainActivity.this, SharepathActivity.class);
//                Intent intent = new Intent(MainActivity.this, WifiActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        checkGPSEnabled();
                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        //checkGPSEnabled();
                        requestLocationPermission();
                    }
                }
            }
        }
    }

    public void checkGPSEnabled() {
        final LocationManager manager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
        if (isDialogDisabled && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            isDialogDisabled = false;
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);

            if (Locale.getDefault().getDisplayLanguage().equals("ไทย")) {
                builder.setMessage("คุณยังไม่ได้เปิด GPS , คุณต้องการเปิด GPS หรือไม่ ?")
                        .setCancelable(false)
                        .setPositiveButton("ใช่", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                isDialogDisabled = true;
                            }
                        })
                        .setNegativeButton("ไม่ใช่", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                dialog.cancel();
                                isDialogDisabled = true;
                                finish();
                            }
                        });
            } else {
                builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                isDialogDisabled = true;
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                dialog.cancel();
                                isDialogDisabled = true;
                                finish();
                            }
                        });
            }


//            builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
//                    .setCancelable(false)
//                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        public void onClick(final DialogInterface dialog, final int id) {
//                            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                            isDialogDisabled = true;
//                        }
//                    })
//                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                        public void onClick(final DialogInterface dialog, final int id) {
//                            dialog.cancel();
//                            isDialogDisabled = true;
//                            finish();
//                        }
//                    });
            final AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public boolean validLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    public void requestLocationPermission() {
        // Here, thisActivity is the current activity
        if (!validLocationPermission()) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation,
                /*
                    try again to request the permission. !!!
                 */

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            } else {
                // No explanation needed; request the permission

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.

                /*
                    first request the permission. !!!
                 */
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        }
        // Permission has already been granted
    }

    @Override
    public void onResume() {
        super.onResume();
        checkGPSEnabled();
    }

    public void checkTalkbackEnable(){
        if (!isExploreByTouchEnabled) {
            final Dialog dialog = new Dialog(MainActivity.this);
            dialog.setContentView(R.layout.confirm_dialog);

            final TextView message = (TextView) dialog.findViewById(R.id.txt_dia);
            Button buttonConfirm = (Button) dialog.findViewById(R.id.btn_yes);
            Button buttonCancel = (Button) dialog.findViewById(R.id.btn_no);

            if (Locale.getDefault().getDisplayLanguage().equals("ไทย")) {
                message.setText(getString(R.string.thtalk));
                buttonConfirm.setText("ใช่");
                buttonCancel.setText("ไม่ใช่");
             } else {
                message.setText(getString(R.string.entalk));
                buttonConfirm.setText("Yes");
                buttonCancel.setText("No");

            }


//            message.setText(getString(R.string.entalk));

            buttonConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                    startActivity(intent);
                    dialog.dismiss();
                }
            });

            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //close popup
                    dialog.dismiss();

                    //exit app
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        finishAndRemoveTask();
                    } else {
                        finish();
                    }
                    Process.killProcess(Process.myPid());
                }
            });

            dialog.setCancelable(false);

            dialog.show();
        }
    }
}
