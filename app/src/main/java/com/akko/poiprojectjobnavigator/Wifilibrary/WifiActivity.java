package com.akko.poiprojectjobnavigator.Wifilibrary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.akko.poiprojectjobnavigator.MainActivity;
import com.akko.poiprojectjobnavigator.NitroLibrary.ui.transfer.TransferActivity;
import com.akko.poiprojectjobnavigator.R;
import com.akko.poiprojectjobnavigator.SettingActivity;
import com.akko.poiprojectjobnavigator.SharepathActivity;
import com.akko.poiprojectjobnavigator.UtilPreference.SharedPreferencesUtil;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

//wifi android tutorial , Enable Disable Wifi , Create Broadcast Receiver , Discover,Connect to peer ,
// Wifi direct data transfer Server Thread,    Wifi direct data transfer client thread, Wifi direct data transfer Send Receiver

public class WifiActivity extends AppCompatActivity {

    private Button toMain;

    int sendCount;
    Button btnOnOff, btnDiscover, btnSend;
    ListView listView;
    TextView read_msg_box, connectionStatus;
    EditText writeMsg;


    WifiManager wifiManager;
    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;

    BroadcastReceiver mReceiver;
    IntentFilter mIntentFilter;

    List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
    String[] deviceNameArray;
    WifiP2pDevice[] deviceArray;

    static final int MESSAGE_READ = 1;

    ServerClass serverClass;
    ClientClass clientClass;
    SendReceive sendReceive;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wifi_activity_wifi);

        initialWork();
        exqListener();
////////////////// ทำการ force wifi ให้เป็น ON
        wifiManager.setWifiEnabled(false);
        wifiManager.setWifiEnabled(true);
//ตัดปุ่มไม่จำเป็นทิ้งไปก่อน        btnOnOff.setText("ON");
///////////      String myDeviceName = DeviceName.getDeviceName();
///////////      setDeviceName(mManager,mChannel,"NAVTU"+myDeviceName);
        setDeviceName(mManager, mChannel, "NAVTU");
        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                if (SharedPreferencesUtil.getValue(WifiActivity.this, "LANG").equals("en")) {

                    connectionStatus.setText("Discovery Started");
                } else {

                    connectionStatus.setText("กำลังค้นหาอุปกรณ์");

                }
            }

            @Override
            public void onFailure(int i) {
                if (SharedPreferencesUtil.getValue(WifiActivity.this, "LANG").equals("en")) {

                    connectionStatus.setText("Discovery Starting Failed");
                } else {

                    connectionStatus.setText("ไม่สามารถเริ่มค้นหาอุปกรณ์ได้");

                }
            }
        });


//////////////////////////////////////////////////
    }

//    Handler handler = new Handler(new Handler.Callback() {                ตััดปุ่มไม่จำเป็นทิ้งก่อน
//        @Override                                                            .
//        public boolean handleMessage(Message msg) {                           .
//            switch (msg.what) {                                                .
//                case MESSAGE_READ:                                             .
//                    byte[] readBuff = (byte[]) msg.obj;                       .
//                    String tempMsg = new String(readBuff, 0, msg.arg1);        .
//                    read_msg_box.setText(tempMsg);                             .
//                    break;                                                      .
//            }                                                                    .
//                                                                              .
//            return true;                                                       .
//        }                                                                     .
//    });                                                                        ตััดปุ่มไม่จำเป็นทิ้งก่อน

    private void exqListener() {
//        btnOnOff.setOnClickListener(new View.OnClickListener() {              ตัดปุ่มไม่จำเป็นทิ้งไปก่อน
//            @Override                                                         .
//            public void onClick(View view) {                                  .
//                if (wifiManager.isWifiEnabled()) {                            .
//                    wifiManager.setWifiEnabled(false);                        .
//                    btnOnOff.setText("ON");                                   .
//                } else {                                                       .
//                    wifiManager.setWifiEnabled(true);                         .
//                    btnOnOff.setText("OFF");                                  .
//                }                                                              .
//            }                                                                .
//        });                                                                ตัดปุ่มไม่จำเป็นทิ้งไปก่อน
        btnDiscover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {

                        if (SharedPreferencesUtil.getValue(WifiActivity.this, "LANG").equals("en")) {

                            connectionStatus.setText("Discovery Started");
                        } else {

                            connectionStatus.setText("กำลังค้นหาอุปกรณ์");

                        }


                    }

                    @Override
                    public void onFailure(int i) {

                        if (SharedPreferencesUtil.getValue(WifiActivity.this, "LANG").equals("en")) {

                            connectionStatus.setText("Discovery Starting Failed");
                        } else {

                            connectionStatus.setText("ไม่สามารถเริ่มค้นหาอุปกรณ์ได้");

                        }
                    }
                });
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final WifiP2pDevice device = deviceArray[i];
                WifiP2pConfig config = new WifiP2pConfig();
                config.deviceAddress = device.deviceAddress;
                //////////////////////////////////////////
                config.wps.setup = WpsInfo.PBC;
                //add this on listview itemClickListener under config.deviceAddress = device.deviceAddress;

                if (android.os.Build.VERSION.SDK_INT > 9) {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                } //then add these on onCreate
                //////////////////////////////////////////

                mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {


                    @Override
                    public void onSuccess() {
                        if (SharedPreferencesUtil.getValue(WifiActivity.this, "LANG").equals("en")) {

                            Toast.makeText(getApplicationContext(), "Connected to " + device.deviceName, Toast.LENGTH_SHORT).show();
                        } else {

                            Toast.makeText(getApplicationContext(), "เชื่อมต่อกับอุปกรณ์ " + device.deviceName, Toast.LENGTH_SHORT).show();

                        }

//                        Toast.makeText(getApplicationContext(), "Connected to " + device.deviceName, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int i) {

                        if (SharedPreferencesUtil.getValue(WifiActivity.this, "LANG").equals("en")) {


                            Toast.makeText(getApplicationContext(), "Not Connected", Toast.LENGTH_SHORT).show();
                        } else {


                            Toast.makeText(getApplicationContext(), "ไม่ได้เชื่อมต่ออุปกรณ์", Toast.LENGTH_SHORT).show();

                        }

//                        Toast.makeText(getApplicationContext(), "Not Connected", Toast.LENGTH_SHORT).show();
                    }






                });
            }
        });

//        btnSend.setOnClickListener(new View.OnClickListener() {       ตัดปุ่มไม่จำเป็นทิ้งก่อน
//            @Override                                                      .
//            public void onClick(View view) {                               .
//                String msg = writeMsg.getText().toString();                .
//                sendReceive.write(msg.getBytes());                        .
//            }                                                             .
//        });                                                           ตัดปุ่มไม่จำเป็นทิ้งก่อน

    }

    private void initialWork() {
//      btnOnOff = (Button) findViewById(R.id.onOff);
        btnDiscover = (Button) findViewById(R.id.discover);
        listView = (ListView) findViewById(R.id.peerListView);
        connectionStatus = (TextView) findViewById(R.id.connectionStatus);
//      writeMsg = (EditText) findViewById(R.id.writeMsg);
//      btnSend = (Button) findViewById(R.id.sendButton);
//      read_msg_box = (TextView) findViewById(R.id.readMsg);

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);

        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        if (SharedPreferencesUtil.getValue(WifiActivity.this, "LANG").equals("en")) {

            connectionStatus.setText("Connection status");
            btnDiscover.setText("DISCOVER REFRESH");
        } else {

            connectionStatus.setText("สถานะการเชื่อมต่อ");
            btnDiscover.setText("กดเพื่อค้นหาอุปกรณ์อีกครั้ง");
        }

    }

    WifiP2pManager.PeerListListener peerListListener = new WifiP2pManager.PeerListListener() {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peerList) {
            if (!peerList.getDeviceList().equals(peers)) {
                peers.clear();
                peers.addAll(peerList.getDeviceList());

                deviceNameArray = new String[peerList.getDeviceList().size()];
                deviceArray = new WifiP2pDevice[peerList.getDeviceList().size()];
                int index = 0;

                for (WifiP2pDevice device : peerList.getDeviceList()) {
                    deviceNameArray[index] = device.deviceName;
                    deviceArray[index] = device;
                    index++;
                }
//

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, deviceNameArray);
                listView.setAdapter(adapter);

            }
            if (peers.size() == 0) {

                if (SharedPreferencesUtil.getValue(WifiActivity.this, "LANG").equals("en")) {


                    Toast.makeText(getApplicationContext(), "No Device Found", Toast.LENGTH_SHORT).show();
                } else {


                    Toast.makeText(getApplicationContext(), "ไม่พบอุปกรณ์", Toast.LENGTH_SHORT).show();
                }

//                Toast.makeText(getApplicationContext(), "No Device Found", Toast.LENGTH_SHORT).show();
                return;
            }
        }
    };


    WifiP2pManager.ConnectionInfoListener connectionInfoListener = new WifiP2pManager.ConnectionInfoListener() {
        @Override
        public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {
            final InetAddress groupOwnerAddress = wifiP2pInfo.groupOwnerAddress;
            if (wifiP2pInfo.groupFormed && wifiP2pInfo.isGroupOwner) {
                if (SharedPreferencesUtil.getValue(WifiActivity.this, "LANG").equals("en")) {


                    connectionStatus.setText("HOST");
                } else {


                    connectionStatus.setText("โฮสต์");
                }
//                    connectionStatus.setText("HOST");
                serverClass = new ServerClass();
                serverClass.start();

///////////////// หลังเชื่อมต่อย้ายไปหน้าส่่งไฟล์
                Intent intent = new Intent(WifiActivity.this, TransferActivity.class);
                startActivity(intent);
                finish();
///////////////////////////////////////////////////////////
            } else if (wifiP2pInfo.groupFormed) {
                if (SharedPreferencesUtil.getValue(WifiActivity.this, "LANG").equals("en")) {


                    connectionStatus.setText("Client");
                } else {


                    connectionStatus.setText("ไคลเอนต์");
                }
//                connectionStatus.setText("Client");
                clientClass = new ClientClass(groupOwnerAddress);
                clientClass.start();
// ////////////////////////
                Intent intent = new Intent(WifiActivity.this, TransferActivity.class);
                startActivity(intent);
                finish();
////////////////////////////////////////
            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mReceiver, mIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    public class ServerClass extends Thread {
        Socket socket;
        ServerSocket serverSocket;

        @Override
        public void run() {
            try {
                serverSocket = new ServerSocket(8881);
                socket = serverSocket.accept();
                sendReceive = new SendReceive(socket);
                sendReceive.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private class SendReceive extends Thread {
        private Socket socket;
        private InputStream inputStream;
        private OutputStream outputStream;

        public SendReceive(Socket skt) {
            socket = skt;
            try {
                inputStream = socket.getInputStream();
                outputStream = socket.getOutputStream();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            byte[] buffer = new byte[1024];
            int bytes;

            while (socket != null) {
                try {
                    bytes = inputStream.read(buffer);
                    if (bytes > 0) {
//                        handler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();    ตัดปุ่มไม่จำเป็นทิ้งก่อน
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public void write(final byte[] bytes) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        outputStream.write(bytes);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
//            try {
//                outputStream.write(bytes);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
    }

    public class ClientClass extends Thread {
        Socket socket;
        String hostAdd;

        public ClientClass(InetAddress hostAddress) {
            hostAdd = hostAddress.getHostAddress();
            socket = new Socket();
        }

        @Override
        public void run() {
            try {
                socket.connect(new InetSocketAddress(hostAdd, 8881), 500);
                sendReceive = new SendReceive(socket);
                sendReceive.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(WifiActivity.this, SharepathActivity.class);
        startActivity(intent);
        finish();
    }

    ////////////////////////////////////////
    /*
Set WifiP2p Device Name
 */
    public void setDeviceName(WifiP2pManager manager, WifiP2pManager.Channel channel, String deviceName) {
        //set device name programatically
        try {
            Class[] paramTypes = new Class[3];
            paramTypes[0] = WifiP2pManager.Channel.class;
            paramTypes[1] = String.class;
            paramTypes[2] = WifiP2pManager.ActionListener.class;
            Method setDeviceName = manager.getClass().getMethod(
                    "setDeviceName", paramTypes);
            setDeviceName.setAccessible(true);

            Object arglist[] = new Object[3];
            arglist[0] = channel;
            arglist[1] = deviceName;
            arglist[2] = new WifiP2pManager.ActionListener() {

                @Override
                public void onSuccess() {
                }

                @Override
                public void onFailure(int reason) {
                }
            };

            setDeviceName.invoke(manager, arglist);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
//    ///////////////////////////////////////////////
}
