package com.akko.poiprojectjobnavigator;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.akko.poiprojectjobnavigator.UtilPreference.SharedPreferencesUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

//  This class use for setting menu
public class SettingActivity extends AppCompatActivity {

    private ListView settingList;
    private Button toMain;
    @BindView(R.id.talkbackbtn)
    Button talkbackbtn;
    @BindView(R.id.navigatetypebtn)
    Button navigatetypebtn;
    @BindView(R.id.optionbtn)
    Button optionbtn;
    @BindView(R.id.modemapbtn)
    Button modemapbtn;
    @BindView(R.id.cameramodebtn)
    Button cameramodebtn;
    @BindView(R.id.aboutbtn)
    Button aboutbtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

//      settingList = (ListView) findViewById(R.id.settingList);
        toMain = (Button) findViewById(R.id.back);
//      String about = getString(R.string.about);
//      initAdapter();


//      Auto detect language
        if (SharedPreferencesUtil.getValue(SettingActivity.this, "LANG").equals("en")) {
            toMain.setText("ฺBACK");
            talkbackbtn.setText("TalkBackSetting");
            navigatetypebtn.setText("NavigateType");
            optionbtn.setText("OptionPlus");
            modemapbtn.setText("MapMode");
            cameramodebtn.setText("CameraMode");
            aboutbtn.setText("About");
        } else {
            toMain.setText("ย้อนกลับ");
            talkbackbtn.setText("เปิด,ปิด Talkback");
            navigatetypebtn.setText("การระบุทิศทาง");
            optionbtn.setText("อุปกรณ์เสริม");
            modemapbtn.setText("โหมดแผนที่");
            cameramodebtn.setText("โหมดกล้อง");
            aboutbtn.setText("เกี่ยวกับ");
        }

//      Set button
        toMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingActivity.this, MainActivity.class);
                intent.putExtra("finished", "");
                intent.putExtra("from", SettingActivity.class);
                startActivity(intent);
                finish();
            }
        });

        talkbackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                startActivity(intent);
            }
        });


    }
//      Set default back button

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SettingActivity.this, MainActivity.class);
        intent.putExtra("finished", "");
        intent.putExtra("from", SettingActivity.class);
        startActivity(intent);
        finish();
    }






}
